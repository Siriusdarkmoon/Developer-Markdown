---
title: 'Updating Documentation'
taxonomy:
    category:
        - docs
visible: true
---

To update the documentation of this site you will need to determine if you are going to update content or specific things about the site. Site content and site look and feel are separate projects. The following sections exist in specific categories:

* Site content
  * Developer documentation
  * ClearOS Installation documentation
  * ClearOS Application and Marketplace Item documentation
  * ClearOS Application Incubation and Skunkworks
* Site Framework (Look and Feel)

## Updating Content of this Site
To modify this site's content which includes the developer documentation and other elements:
* create an issue against the [developer documentation project](https://gitlab.com/clearos/docs/Developer-Markdown)
* clone the [ClearOS Developer Docs Project](hhttps://gitlab.com/clearos/docs/Developer-Markdown)
* modify the code in your own space and merge it, or if you are an internal developer commit it, back to GitLab
* Validate the change which should go into effect immediately upon commit to master in GitLab.


## ClearOS Installation Documentation
ClearOS Installation Documentation has not been migrated to Gitlab.

## ClearOS Application and Marketplace Item Documentation
ClearOS Application and Marketplace Documentation has not been migrated to Gitlab.

## ClearOS Best Practices, Application Incubation, and Skunkworks
ClearOS Best Practices, Application Incubation, and Skunkworks Documentation (howtos and knowledgebase) has not yet been migrated to Gitlab.

## Updating Site Framework (Look and Feel)
To modify this site's information you will need to:
* create an issue against the [project](https://gitlab.com/clearos/docs/clearos-developer-docs/issues)
* checkout the [ClearOS Developer Docs Project](https://gitlab.com/clearos/docs/clearos-developer-docs)
* modify the code and commit it, or merge it, back to GitLab
* have a team member pull it down to the docs.clearos.com website

'''
cd /home/centos/clearos-developer-docs
git pull git@gitlab.com:clearos/docs/clearos-developer-docs.git master
'''
