---
title: 'Source Code'
media_order: 'gitlab_fork.png,gitlab_users.png'
taxonomy:
    category:
        - docs
visible: true
---

## Forking the GitLab Source Code Repository

Now that we havd the package installed, along with all dependencies and whatever install/upgrade scripts might result of the install, it's time to clone the Git repository.  This will allow us to work in a sandboxed folder using Webconfig's port 1501 developer access to see our changes in realtime.

If you're reading this, it's quite likely that you are not yet a member of the ClearOS core developer team, you can checkout/clone the code, but you wouldn't have permission to push commits.

!!! We encourage you to send us a request as soon as two pull requests are accepted by the team.  It makes your job easier, and ours!

To get around this limitation, you are going to fork the code to your own GitLab account.

Login to your (possibly newly) created GitLab account and then head over to the ClearOS main project on GitLab:

[https://gitlab.com/clearos](https://gitlab.com/clearos)

Find your project and navigate to it.  In our case, we want the project app-users found [here](https://gitlab.com/clearos/clearfoundation/app-users).  Click on Fork:

![GitLab Fork](gitlab_fork.png)

You will be taken to a list of GitLab namespaces (sandboxes) in which you can fork an exact duplicate of the ClearOS source code project.

Select any namespace available to you...most likely, your private GitLab account.  Once done, you will be redirected to the new home of the app-users app (or whichever project you're working on) that you have full permissions to read/write to.

## Cloning the GitLab Source Code Repository

If you've set up your private keys (either on your workstation and have them in your keychain or if you put your key directly on the server in your development environment), select SSH and click on the clipboard icon to copy the URL to your clipboard.

![Clone URL](gitlab_users.png)

!!! If you're struggling with your SSL keys - don't worry.  Use the HTTPS URL...it just means you'll have to enter your username/password during checkouts and commits or set up [GitLab's Personal Access Tokens](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html).

Time to checkout/clone the source code from the Git repo:

    cd ~/apps
    git clone git@gitlab.com:PERSONAL_GIT_REPOSITORY/app-users.git users
    cd users

! Note the specifying of the folder 'users' in the git clone statement above.  Always remove the "-app" prefix and convert dashes (-) to underscore (\_) when cloning repositories to conform to the standard used by ClearOS apps in the framework.

If you list the folders/files in your current working directory, you should now see something like this:

    ls -l
    total 8
    drwxr-xr-x  2 root root   50 Aug  1 13:57 controllers
    drwxr-xr-x  2 root root   37 Aug  1 13:57 deploy
    drwxr-xr-x  2 root root   46 Aug  1 13:57 htdocs
    drwxr-xr-x 88 root root 4096 Aug  1 13:57 language
    drwxr-xr-x  2 root root  219 Aug  1 13:57 libraries
    drwxr-xr-x  2 root root   92 Aug  1 13:57 packaging
    -rw-r--r--  1 root root  999 Aug  1 13:57 unify.json
    drwxr-xr-x  3 root root   58 Aug  1 13:57 views

