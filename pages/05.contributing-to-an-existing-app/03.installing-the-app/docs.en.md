---
title: 'Installing the App'
taxonomy:
    category:
        - docs
visible: true
---

## Installing the App
On your development environment, you may need to install the app already so that you can be sure to have all the libraries of other apps which might be required so that you can get a proper view of the app while you develop.

In our example, we know that the users app is a core app...installed by default during the installation process with physical media, so we **don't** need to run the following since it's already installed (won't hurt though):

   yum -y install app-users
   
If the app you're contributing to is not core, install it from the Marketplace or via command line.