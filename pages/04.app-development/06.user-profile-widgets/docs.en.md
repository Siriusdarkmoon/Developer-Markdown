---
title: 'User Dashboard Widgets'
media_order: syncthing_user_profile.png
taxonomy:
    category:
        - docs
visible: true
---

User Dashboard widgets are provided by the app when you install them (and removed if you un-install the app).  They are useful in conveying information to end-users who login to Webconfig and are looking for status or direction on a particular app.

One typical use case is to show the status and provide a link to additional, web-based applications - point in question: SyncThing.
    
## Writing the Controller/View

Unlike a admin dashboard widget, a user dashboard controller is not constrained under a naming convention.  Typically, the name of the controller would end in **\_widget** to convey this use case.

You can have any number of controllers show up under a user's dashboard for any given app.

Let's say you want to display some information to a user by creating a controller as follows:

    access_widget.php.

This controller can pull, like any other controller, information out of the ClearOS models for end display in a view you also create.

## Making the User Dashboard Widget Available

Once your controller and view(s) have been completed, you ned to hook into the Webconfig framework.  To do this, simply edit the <code>/deploy/info.php</code> app definition/metadata file and add:

    /////////////////////////////////////////////////////////////////////////////
    // User Dashboard Widgets
    /////////////////////////////////////////////////////////////////////////////

    $app['user_dashboard_widgets'] = [
        'syncthing/access_widget',
    ];

where you would replace syncthing (the base app in this example) with your app and the specific name of your controller.  As you can see, the user_dashboard_widget attribute accepts an array of controllers that allow you to add as much content as you wish.

## Use Case

As mentioned above, this extension to the framework can be useful for display information to users on their dashboard.  For the case of Syncthing, it servers as a status indicator and reminder of where they can access their Syncthing account to manager/configure their settings.

![User Dashboard Example](syncthing_user_profile.png)