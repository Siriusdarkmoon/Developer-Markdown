---
title: MVC
visible: true
taxonomy:
    category: docs
---

## Directory Layout
For those of you already familiar with a Model-View-Controller (MVC) pattern, the directory layout below should look familiar.

| Directory         | Description                                                                     |
| ----------------- | ------------------------------------------------------------------------------- |
| controllers       | The “C” in MVC (Model-View-Controller)                                          |
| deploy            | App information, install scripts, default configuration and other helper files  |
| htdocs            | Javascript, images and other web assets                                         |
| language          | Translations                                                                    |
| libraries         | The “M” in MVC (ClearOS only uses libraries - similar to models)                |
| packaging         | Package information                                                             |
| tests             | Unit tests                                                                      |
| views             | The “V” in MVC                                                                  |

## Creating the MVC Folders

Don't worry about having to create this folder structure if you're working on a new app.  The clearos CLI script makes things easy:

    cd ~/apps
    clearos newapp
    
Enter your app's basename (all lowercase, with underscore word separators), and the utility will not only create the directory structure, but also stub out your basic controller and view.