---
title: Daemons
visible: true
taxonomy:
    category: docs
---

Not all apps require daemons, however, when they do, you'll almost certainly want to expose basic functionality of a daemon like whether it's running or not, and starting and stopping the service.

### Daemon Controller

In the app base (a default app that is available in all ClearOS instances), you'll find a default "Daemon" Controller that you can use to extend off your app's daemon class.  We can call the controller anything, however, sticking to convention, we'd suggest using the filename server.php to container your daemon definition.

    /controllers/server.php
    
Let's look at an example - the SMTP (Postfix) daemon instance.

    <?php

    /**
     * Postfix daemon controller.
     *
     * @category   apps
     * @package    smtp
     * @subpackage controllers
     * @author     ClearFoundation <developer@clearfoundation.com>
     * @copyright  2011 ClearFoundation
     * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
     * @link       http://www.clearfoundation.com/docs/developer/apps/smtp/
     */

    ///////////////////////////////////////////////////////////////////////////////
    //
    // This program is free software: you can redistribute it and/or modify
    // it under the terms of the GNU General Public License as published by
    // the Free Software Foundation, either version 3 of the License, or
    // (at your option) any later version.
    //
    // This program is distributed in the hope that it will be useful,
    // but WITHOUT ANY WARRANTY; without even the implied warranty of
    // MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    // GNU General Public License for more details.
    //
    // You should have received a copy of the GNU General Public License
    // along with this program.  If not, see <http://www.gnu.org/licenses/>.
    //
    ///////////////////////////////////////////////////////////////////////////////

    ///////////////////////////////////////////////////////////////////////////////
    // B O O T S T R A P
    ///////////////////////////////////////////////////////////////////////////////

    $bootstrap = getenv('CLEAROS_BOOTSTRAP') ? getenv('CLEAROS_BOOTSTRAP') : '/usr/clearos/framework/shared';
    require_once $bootstrap . '/bootstrap.php';

    ///////////////////////////////////////////////////////////////////////////////
    // D E P E N D E N C I E S
    ///////////////////////////////////////////////////////////////////////////////

    require clearos_app_base('base') . '/controllers/daemon.php';

    ///////////////////////////////////////////////////////////////////////////////
    // C L A S S
    ///////////////////////////////////////////////////////////////////////////////

    /**
     * Postfix daemon controller.
     *
     * @category   apps
     * @package    smtp
     * @subpackage controllers
     * @author     ClearFoundation <developer@clearfoundation.com>
     * @copyright  2011 ClearFoundation
     * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
     * @link       http://www.clearfoundation.com/docs/developer/apps/smtp/
     */

    class Server extends Daemon
    {
        /**
         * Postfix daemon constructor.
         */

        function __construct()
        {
            parent::__construct('postfix', 'smtp');
        }
    }

Pretty straightforward.  Aside from some of the packaging comments, the only thing you need to change in your app is the parent contructor.

The first parameter is the daemon (or process) name (not to be confused with the package name which may be different).

The second parameter is the app package basename.  The wrapper for the Postfix app is named app-smtp, so the basename is just 'smtp' which we see in the controller.

In order to display the status and start/stop buttons in Webconfig, simply add your Server.php controller to your default route.

    <?php
    ...
    ..
    $views = array('smtp/server', 'smtp/settings', 'smtp/trusted', 'smtp/forwarding');
    ...
    ..
    $this->page->view_forms($views, lang('smtp_smtp_server'));
    

### Daemon Metadata File

In most cases, you'll need to create a ClearOS Daemon configlet file.  It contains bits of information that provides customization.

#### Configlet Format

The Postfix daemon configlet is shown below.  The naming of this file is important.  It **must** be named the same as the daemon name as defined in the parent contructor of the controller (see above).  In this case, <code>postfix.php</code>.

    <?php

    ///////////////////////////////////////////////////////////////////////////////
    // B O O T S T R A P
    ///////////////////////////////////////////////////////////////////////////////

    $bootstrap = getenv('CLEAROS_BOOTSTRAP') ? getenv('CLEAROS_BOOTSTRAP') : '/usr/clearos/framework/shared';
    require_once $bootstrap . '/bootstrap.php';

    ///////////////////////////////////////////////////////////////////////////////
    // T R A N S L A T I O N S
    ///////////////////////////////////////////////////////////////////////////////

    clearos_load_language('smtp');

    ///////////////////////////////////////////////////////////////////////////////
    // C O N F I G L E T
    ///////////////////////////////////////////////////////////////////////////////

    $configlet = array(
        'title' => lang('smtp_app_name'),
        'package' => 'postfix',
        'process_name' => 'master',
        'pid_file' => '/var/spool/postfix/pid/master.pid',
        'reloadable' => TRUE,
        'url' => '/app/smtp'
    );

| Parameter  | Required | Description                                                          |
| ---------- | -------- | -------------------------------------------------------------------- |
| title      | Yes      | Title                                                                |
| package    | Yes      | Package name                                                         |
| pid_file   | No       | Name/location of the process PID file                                |
| reloadable | No       | Flag wwhether 'reload' is an option instead of restarting the daemon |
| url        | No       | Webconfig route to configure                                         |

This file must reside in:

    /var/clearos/base/daemon/

#### Packaging the Configlet
As with any supporting file, you'll add this file to the /packaging/ folder in your development environment.  To copy this file to the daemon folder noted above, simply add to the <code>core_file_manifest</code> parameter in your /deploy/info.php file:

    $app['core_file_manifest'] = array(
        'postfix.php'=> array('target' => '/var/clearos/base/daemon/postfix.php'),
        ...
        ..
        .
    );

## Multiple Daemons
In some rare cases, your app may have more than one daemon that you wish to monitor.  In this case, extending the Daemon controller from the base is not possible, since it was designed to present a single service.

An example of an app having multiple daemons to present to the user, is the Kopano Mail Collaboration suite.  The application splits services into:
* kopano-dagent
* kopano-gateway
* kopano-ical
* kopano-monitor
* kopano-server
* kopano-spooler
* kopano-search

If your app requires more than a single daemon, you'll need to create your own services controller.  An example for Kopano is listed below.  Note the use of the Kopano library that fetches the services list.

    <?php

    /**
     * Kopano services controller.
     *
     * @category   apps
     * @package    kopano
     * @subpackage controllers
     * @author     ClearFoundation <developer@clearfoundation.com>
     * @copyright  2016 ClearFoundation
     * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
     * @link       http://www.clearfoundation.com/docs/developer/apps/kopano/
     */

    ///////////////////////////////////////////////////////////////////////////////
    //
    // This program is free software: you can redistribute it and/or modify
    // it under the terms of the GNU General Public License as published by
    // the Free Software Foundation, either version 3 of the License, or
    // (at your option) any later version.
    //
    // This program is distributed in the hope that it will be useful,
    // but WITHOUT ANY WARRANTY; without even the implied warranty of
    // MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    // GNU General Public License for more details.
    //
    // You should have received a copy of the GNU General Public License
    // along with this program.  If not, see <http://www.gnu.org/licenses/>.
    //
    ///////////////////////////////////////////////////////////////////////////////

    ///////////////////////////////////////////////////////////////////////////////
    // C L A S S
    ///////////////////////////////////////////////////////////////////////////////

    /**
     * Kopano services controller.
     *
     * @category   apps
     * @package    kopano
     * @subpackage controllers
     * @author     ClearFoundation <developer@clearfoundation.com>
     * @copyright  2016 ClearFoundation
     * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
     * @link       http://www.clearfoundation.com/docs/developer/apps/kopano/
     */

    class Kopano_Services extends ClearOS_Controller
    {
        protected $app_name = NULL;

        /**
         * Daemon constructor.
         *
         * @param string $app_name    app that manages the daemon
         *
         * @return view
         */

        function __construct($app_name)
        {
            $this->app_name = $app_name;
        }

        /**
         * Default daemon controller.
         *
         * @return view
         */

        function index()
        {
            // Load dependencies
            //------------------

            $this->lang->load('kopano');
            $this->load->library('kopano/Kopano');

            // Show account status widget if we're not in a happy state
            //---------------------------------------------------------

            $this->load->module('accounts/status');

            if ($this->status->unhappy()) {
                $this->status->widget('users');
                return;
            }

            // Load view data
            //---------------

            try {
                $data['form_type'] = $form_type;
                $data['basename'] = $this->app_name;
                $data['services'] = $this->kopano->get_services_info();
            } catch (Exception $e) {
                $this->page->view_exception($e);
                return;
            }

            // Load views
            //-----------

            $this->page->view_form('kopano/services', $data, lang('kopano_kopano_services'));
        }

        /**
         * Daemon start.
         *
         * @return view
         */

        function start($daemon)
        {
            $this->load->library('base/Daemon', $daemon);

            try {
                $this->daemon->set_running_state(TRUE);
                $this->daemon->set_boot_state(TRUE);

                redirect('/' . $this->app_name . '/');
            } catch (Exception $e) {
                $this->page->view_exception($e);
                return;
            }
        }

        /**
         * Daemon stop.
         *
         * @return view
         */

        function stop($daemon)
        {
            $this->load->library('base/Daemon', $daemon);

            try {
                $this->daemon->set_running_state(FALSE);
                $this->daemon->set_boot_state(FALSE);

                redirect('/' . $this->app_name . '/');
            } catch (Exception $e) {
                $this->page->view_exception($e);
                return;
            }
        }
    }
    
Of course, using this method will not place the daemons in the right hand side widget, but instead, a table in the main section of the app's configuration.

## Multi-User

TODO - Explain method/limitations on multi-user apps like Dropbox and Syncthing.