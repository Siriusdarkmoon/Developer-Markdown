---
title: 'ClearOS Framework'
taxonomy:
    category:
        - docs
visible: true
---

ClearOS's Webconfig is comprised of components written in a number of languages:
* PHP
* JavaScript/JQuery
* C++
* Bash scripts
* Python
* Lua

to name a few.  Don't worry...you don't need to be a master of all in order to develop on ClearOS.

The main engine behind Webconfig is PHP.  ClearOS uses the [CodeIgniter PHP framework](https://www.codeigniter.com/) to make life easier.  If you're inclined to support existing apps or build new ones, you'll want to familiarize yourself with CodeIgniter and go through some of the [docs](https://www.codeigniter.com/user_guide/) before jumping into ClearOS.  Among other things, CodeIgniter provides the MVC design principles to make efficient use of code and allows separate of login and UI presentation.

## Framework Location and Version
The CodeIgniter framework can be found in:

    /usr/clearos/framework
    
ClearOS currently uses CI v3.1.4.
