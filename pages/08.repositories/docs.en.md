---
title: Repositories
visible: true
taxonomy:
    category: docs
---

ClearOS hosts mirrors around the world to provide downloading of the ISO images and host the RPM repositories for adding and updating software.

The master mirror list can be found [here](http://master-mirror.clearos.com/clearos/mirrorlist/).