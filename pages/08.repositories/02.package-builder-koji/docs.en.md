---
title: 'Package Builder (Koji)'
visible: true
taxonomy:
    category: docs
---

Koji is the software that automagically builds ClearOS packages.  When you commit to a valid git repository as described in section TODO that triggers a Koji build webhook, the process of building a package in a clean environment will start automatically.

## Monitoring and Visibility
Koji provides visibility into what is happening in the evironment for anyone to view.  To view:
* what packages have recently  been built
* current status of packages being built
* errors/logs preventing a package from building

goto [http://koji.clearos.com/koji/](http://koji.clearos.com/koji/)

## Getting Info
If you are having any issues with the ClearOS Koji Package Builder infrastructure, please send an email to infrastructure@clearcenter.com and someone from the internal team will get back to you shortly.