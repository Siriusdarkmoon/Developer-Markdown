## What is ClearOS?

ClearOS is an operating system platform that will run on dedicated hardware or in your virtual environment in your home, office, or datacenter. ClearOS runs server, network, gateway, private cloud and cloud-connected applications and services designed for the IT environment. For example, you can use ClearOS as:

* a finely tuned system with just a handful of specific apps and services to create a * specific IT solution, or
* an Internet gateway to protect your other computers from the outside internet while * providing your users with a stable, fast web experience
* a server where you store your files for secure access inside and outside your network
* a network platform to facilitate core network services to provide ease of management of your network
* a cloud platform which allows you to both integrate with cloud services or to become your OWN cloud services provider for your workstations
* a versatile all-in-one solution that does everything above, or
* something completely in between!
Click to check out a demo server

## Try it Out
[![clearos7-marketplace-tiles.png](clearos7-marketplace-tiles.png)](https://demo1.clearos.com:81/)

Click [here](https://demo1.clearos.com:81/) to try it out.  
Username: get  
Password: clear

## Show Me More
<a href="http://www.youtube.com/watch?feature=player_embedded&v=9pRP0c_cZvI& " target="blank"><img src="http://img.youtube.com/vi/9pRP0c_cZvI&/0.jpg" alt="IMAGE ALT TEXT HERE" width="480" height="360" border="10" /></a>

Most people will have some use for ClearOS. Many will find it indespensible. If you use digital technology for storing or accessing data, ClearOS has something for you. ClearOS is a modular operating platform and allows for both consolidation of service and tight integration. For example, a 20-person office using ClearOS as a dedicated gateway can provide services that are more lightweight, use less energy, and easier to manage when compared to an office deployment of multiple pieces of hardware and software to the same functionality. In this office scenario, we are able to provide the authentication control among various gateway and network systems which provides ease of management and ease of use for users. For example, they won't have to remember different usernames or passwords for both the VPN and the Content filter. When they change their passwords in the future, it affects all services on the platform. This can aid in compliance to industry standards and to best practices. ClearOS is often compared to Microsoft® Small Business Server and has received many awards for its ease of use.

ClearOS further extends this ease of use by working well with other ClearOS and non-ClearOS servers. The ClearOS landscape favors open standards and mechanisms which equates to better interoperability between other systems and avoids the pitfalls of solutions which lock the user in specific vendors.

## Navigation
 * Next [System Requirements](/installing-clearos/preparation/system-requirements)
