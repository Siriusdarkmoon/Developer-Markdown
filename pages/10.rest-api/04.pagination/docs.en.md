---
title: Pagination
visible: true
---

## Overview
Pagination is not yet implemented for the ClearOS API.  For reference, pagination data will look like the following in the future.

```json
    "status_code": 0,
    "status_message": "Success.",
    "data_metadata": {
        "total_records": 361435,
        "total_pages": 36144,
        "current_page": 1,
        "limit": 10
    },
```