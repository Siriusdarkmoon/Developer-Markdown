---
title: 'REST API'
taxonomy:
    category:
        - docs
visible: true
---

The ClearOS API is built around the REST web services concepts.  Our API includes:

* Predictable resource-oriented URLs
* Standard HTTP response codes along with augmented error codes
* Enhanced validation handling
* Standard authentication
* Cross-origin resource sharing (CORS)
* JSON response payloads
* Discovery

The end points for the ClearOS API can be access on port 83 of your ClearOS system, e.g.

    https://example.org:83/api/v1/network
 
Before you fire up your [Postman](https://www.getpostman.com/) or curl command line to try the ClearOS API, please review the steps needed to get [authentication working](authentication).